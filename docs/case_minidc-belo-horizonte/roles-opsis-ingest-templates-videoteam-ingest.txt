    exec voctomix-ingest \
        --host {{ ingest.host }} \
        --port {{ ingest.port }} \
        --video-source {{ video_source }} \
        --video-attribs device=/dev/video0 \
