.. _voctomix:

Standalone Voctomix Machine
===========================

The simplest setup that is able to record a talk is a webcam connected to a PC
running Voctomix. This can be done as follows:

1. Install Debian Stable on a machine. Make the hostname ``voctomix1``. We
   recommend a minimal install: only the base system and optionally, an SSH
   server. The netinstall image is good for this.

2. Edit ``/root/.ssh/authorized_keys`` and add plublic ssh key to allow connection from my laptop

3. Edit ``/etc/ssh/sshd_config`` and uncomment the line::

    PermitRootLogin prohibit-password

4. Install ansible::

    $ apt install ansible

My laptop
---------

1. Clone repository::

    $ git clone git@salsa.debian.org:debconf-video-team/ansible.git
    
2. Edit ``/etc/hosts`` and add the IP provided by tp-link router to voctomix1::

    192.168.0.101 voctomix1

3. Edit ``inventory/group_vars/all`` and change:

   .. include:: inventory-group_vars-all.txt
       :code:

4. Edit ``inventory/group_vars/onsite`` and change:

   .. include:: inventory-group_vars-onsite.txt
       :code:

5. Edit ``inventory/host_vars/voctomix1`` and change:

   .. include:: inventory-host_vars-voctomix1.txt
       :code:

Ansible can be run from a laptop or other external source. This makes testing
changes much faster than running it locally. You will require a network that
resolves hostnames to IP addresses correctly. It also requires your SSH key to
be present in the ``authorized_keys`` file for root (this will be the case if
your key is in the ``ssh_public_keys`` list and you have run ansible before).
In this case run::

    $ ansible-playbook --inventory inventory/hosts -l voctomix1 site.yml

6. When it completes successfully, restart the machine.

You should now have a working voctomix machine. Since there is only one input at
the moment, you cannot really mix different streams. The next step is to
:ref:`build an opsis machine <opsis>` to get a second input. To do this, you
might need to :ref:`build a gateway machine <gateway>` first.

Streaming
---------

Streaming to services such as YouTube is very easy using this setup. It
requires setting up the stream on YouTube and then configuring Voctomix to
connect to this stream.

1. Open YouTube `Live Dashboard`_.

2. Change the *Name* and *Description*, setting the *Category* and *Privacy* as
   required.

3. Note down the *Server URL* and *Stream name/key* (which will be displayed as
   a hidden password until the *Reveal* button is pressed). These will be used
   to configure Voctomix using Ansible.

4. Create an Ansible Vault file for the vault password::

    $ echo "changeme" > /root/.ansible-vault
    $ chmod 600 /root/.ansible-vault

5. Encrypt the URL and stream name in the form ``<URL>/<Stream name>``::

   $ ansible-vault encrypt_string --vault-password-file=/root/.ansible-vault --name=location <URL>/<Stream name>

6. Edit ``inventory/host_vars/voctomix1.yml`` and include the following,
   changing the bitrates as required::

    streaming:
      method: rtmp
      hq_config:
        video_bitrate: 2500 # kbps
        audio_bitrate: 128000 # bps
        keyframe_period: 60 # seconds
      rtmp:
        vaapi: true
        <Output of ansible-vault encrypt_string>
        location: !vault |.....

7. Rerun ansible::

    $ ansible-playbook --vault-password-file=/root/.ansible-vault --inventory inventory/hosts -l voctomix1 site.yml

Once ansible is finished, opening Voctomix and selecting *Stream Live* or
*Stream Loop* will start streaming to YouTube. If you wish to run more than one
room or schedule when your stream goes live, use the *Event* tab and schedule
the stream. Here you can add cameras for additional rooms (one per room).

.. _`Live Dashboard`: https://www.youtube.com/live_dashboard

Notes
-----

The Voctomix loops are shown on the stream when there is no talk happening. In
Voctomix the ``loop_url`` is downloaded and used for the *Stream Loop* button
when the stream is blanked. ``bgloop_url`` is downloaded and used as the
background for Picture-In-Picture mode when the sources do not cover the entire
frame. If you have loops to use, put the URL for them here::

    voctomix:
      loop_url: http://example.org/loop/loop.ts
      bgloop_url: http://example.org/loop/bgloop.ts
