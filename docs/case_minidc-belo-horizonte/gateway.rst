.. _gateway:

Adding a Gateway
================

For MiniDebConf Belo Horizonte, we used the Router TP-Link.

It provides the IP 192.168.0.101 for voctomix machine and 192.168.0.103 for
capture laptop.
