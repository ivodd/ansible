.. _opsis:

Adding a capture PC
===================

The next step is to add a capture PC/laptop for capturing presentations.

This setup was done previously using a Numato Opsis board. The previous hardware
setup for this can be found :ref:`here <dcvideo:laptop_capture>`.

The basic layout for MiniDebConf Belo Horizonte is:

* The presenter's laptop connects to the capture box CU4K30 Asus input via HDMI.
* The capture box CU4K30 Asus output connects to the projector via HDMI.
* The capture box CU4K30 Asus capture connects to a capture laptop sitting next
to it, via USB-C.
* The capture laptop is connected to the Router TP-Link over a gigabit ethernet
network.
* The Voctomix machine is connected to the Router TP-Link over a gigabit
ethernet network.

1. Install Debian Stable on laptop in the same manner you installed
   the :ref:`Voctomix <voctomix>` machine. Make the hostname ``opsis1``.

2. Install ansible::

    $ apt install ansible

My laptop
---------

1. Clone repository (already done for voctomix setup)::

    $ git clone git@salsa.debian.org:debconf-video-team/ansible.git
    
2. Edit ``/etc/hosts`` and add the IP provided by tp-link router to voctomix1::

    192.168.0.103 opsis1

3. Edit ``inventory/host_vars/opsis1.yml`` and change the following:

   .. include:: inventory-host_vars-opsis1.txt
       :code:

4. Edit ``roles/opsis/defaults/main.yml`` and add the following:

   .. include:: roles-opsis-defaults-main.txt
       :code:

5. Edit ``roles/apt/defaults/main.yml`` and change the following:

   .. include:: roles-apt-defaults-main.txt
       :code:

6. Edit ``roles/opsis-ingest/defaults/main.yml`` and change the following:

   .. include:: roles-opsis-ingest-defaults-main.txt
       :code:

7. Edit ``roles/opsis-ingest/templates/videoteam-ingest.j2`` and change the following:

   .. include:: roles-opsis-ingest-templates-videoteam-ingest.txt
       :code:
         
8. Run::

    $ ansible-playbook --inventory inventory/hosts -l opsis1 site.yml

9. When it completes successfully, restart the machine.

.. _`official documentation`: https://hdmi2usb.tv/firmware/#flashing-prebuilt-firmware
