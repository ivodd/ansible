.. _case_minidc-belo-horizonte:

Case MiniDC Belo Horizonte
==========================


`MiniDebConf Belo Horizonte 2024`_ was held at the Pampulha Campus of UFMG -
Federal University of Minas Gerais, in Belo Horizonte city, from 27th to 30th
of April, 2024.

Hardware:
---------

1. Voctomix (main machine) - cabin in the back of auditorium

   * Desktop Dell Optiplex 7010 - Core i7-13700, RAM 16GB, SSD 512GB

2. Capture PC - table on the stage

   * Laptop Dell XPS 13 9300 - Core i7, 16GB, SSD 1TB
   * Dell Thunderbolt Dock WD19TBS

3. Capture box CU4K30 Asus - table on the stage

4. Gateway - cabin in the back of auditorium

   * Router TP-Link N750

5. Camcorder - hallway in the back of auditorium

   * `Sony HMC-2000`_

We used auditorium B102 at CAD3. There wasn't a mixer, so we connected the cabe
XLR from the `translation room`_ `output`_ direct to the voctomix desktop
input.

The camcorder had only a 3.5mm jack for sound input and It didn't accept the
connection from the output because it was necessary to use a mixer. I tried to
use there my mixer Behringer Henyx Q1002USB but it didin't work.
Later, I tested at my home and `it worked`_.

Room setup video:
-----------------

Connections::

    Presenter's laptop (hdmi) -> Capture box (hdmi)  -> Projector
    Presenter's laptop (hdmi) -> Capture box (usb-c) -> Dock (usb-c) -> Capture PC (network cable) -> Router

Connection::

    Camcorder (hdmi converter usb) -> Main machine

Connection::

    Main machine (network cable) -> Router


Room setup audio:
-----------------

Connection::

    Mic output (XLR cable converter 3.5mm jack) -> Main machine

Links:
------

https://github.com/CarlFK/veyepar/wiki/System-Stack#what-to-do-first

https://debconf-video-team.pages.debian.net/ansible/ansible_roles/voctomix.html

https://salsa.debian.org/debconf-video-team/ansible-inventory

https://salsa.debian.org/debconf-video-team/ansible

https://debconf-video-team.pages.debian.net/ansible

https://github.com/CarlFK/voctomix-outcasts/blob/master/ingest.py


.. toctree::
   :numbered:

   case_minidc-belo-horizonte/voctomix.rst
   case_minidc-belo-horizonte/capture-pc.rst
   case_minidc-belo-horizonte/gateway.rst

.. _`MiniDebConf Belo Horizonte 2024`: https://bh.mini.debconf.org
.. _`Sony HMC-2000`: https://mundodoevento.com.br/produtos/aluguel-de-camera-filmadora-profissional-sony-hmc-2000
.. _`output`: https://photos.google.com/share/AF1QipPZulqRz0JgQAkJb55jK8_htB3pBFKQYrqz5DyC_0wmtRo7MTE5nSOJLDsD7MOjCg/photo/AF1QipMbRc0nSOaGIj4Wlf_LVBFtwgGdI1wyl27hWE4?key=TThaaDBvVGRkVkpvd1o4eWhKRVhmQ1A4bkdlMDF3
.. _`it worked`: https://photos.google.com/share/AF1QipPZulqRz0JgQAkJb55jK8_htB3pBFKQYrqz5DyC_0wmtRo7MTE5nSOJLDsD7MOjCg/photo/AF1QipP-BmkC0vYnn-zuVQneXnpQK_VoLiGpjJf0VvA?key=TThaaDBvVGRkVkpvd1o4eWhKRVhmQ1A4bkdlMDF3
.. _`translation room`: https://photos.google.com/share/AF1QipPZulqRz0JgQAkJb55jK8_htB3pBFKQYrqz5DyC_0wmtRo7MTE5nSOJLDsD7MOjCg/photo/AF1QipMIw8vNbAXj-o1VcuWNa6UZkQ1ODxpx1SQR21I?key=TThaaDBvVGRkVkpvd1o4eWhKRVhmQ1A4bkdlMDF3
