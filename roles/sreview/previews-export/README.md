# sreview/previews-export

This role sets up a nginx virtual host to publish the encoded previews over
http, to allow the sreview frontend to use them.

## Tasks

Everything is in the `tasks/main.yml` file.

## Available variables

Main variables are:

* `sreview.wwwhostname`:        Public hostname under which the previews are to be made
                                available via https.

* `sreview.wwwroot`:            Directory where the previews are made available by sreview.

* `sreview.self_signed_certificate`: Skip Let's Encrypt signing.

* `letsencrypt_well_known_dir`: Directory where to store the `/.well-known/` data
                                for the Let's Encrypt challenge.
