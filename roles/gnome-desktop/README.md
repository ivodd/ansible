# gnome-desktop

Manage a Gnome/Wayland/GDM3-based desktop setup and other related GUI applications

## Tasks

The tasks are divided this way:

* `tasks/packages.yml`: Install packages.

* `tasks/gdm3.yml`: Manages GDM3.

* `tasks/dconf.yml`: Manages Gnome settings through dconf.

* `tasks/nm.yml`: Manages NetworkManager.

## Available variables

Main variables are:

* `user_name`: The username of the main user.

* `autologin`: Boolean. If false, the GDM3 autologin is turned off.

Other variables used are:

* `skip_unit_test`:  Used internally by the test suite to disable actions that
                     can't be performed in the gitlab-ci test runner.
