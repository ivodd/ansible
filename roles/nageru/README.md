# nageru

Manage and configure the nageru live editing software.

Requires VA-API which requires non-free to be enabled for the Intel
drivers.

## Tasks

The tasks are divided this way:

* `tasks/rtmp.yml`: Manage RTMP streaming to the streaming backend machine.

* `tasks/scripts.yml`: Manage useful scripts for the video director.

* `tasks/video_disk.yml`: Manage partitions to record to.

* `tasks/nageru.yml`: Install and configure nageru.

## Available variables

Main variables are:

* `user_name`: Main user username.

* `storage_username`: Storage user username.

* `debian_version`: Version of Debian, when using Debian.

* `org`: Name of your organisation. Used in video files path.

* `show`: Name of the event. Used in the video files path.

* `room_name`: Name of the room where you are recording. Used in the video file
               path.

* `frequency`: The local frequency setting (50 or 60Hz). Used to derive
               sensible defaults.

* `sources`: List. Name of the different sources you want nageru to use.

* `nageru.autostart`: Start nageru when X starts.

* `nageru.http_port`: The port nageru listens on.

* `nageru.loop_url`: URL of the sponsor loop file.

* `nageru.bgimage_url`: URL of the background .png image file.

* `nageru.loopy_url`: URL of the live OBS sponsor loop.

* `recording_timezone`: Optional. Record timestamps in the specified TZ.

* `streaming.method`: Streaming method used. At the moment, only `none` and
                      `rtmp` values are supported.

* `streaming.rtmp.location`: RTMP URL to the streaming endpoint. For YouTube,
                             this would be: `rtmp://a.rtmp.youtube.com/live2/x/SUPER_SECRET_KEY app=live2`

* `rsync_excludes`: List. Paths to exclude during the rsync copy of the video
                    files.

* `rsync_sshopts`: Rsync SSH options.

* `video_disk`: Partition to create and mount on `/srv/video`.

Other variables used are:

* `skip_unit_test`:  Used internally by the test suite to disable actions that
                     can't be performed in the gitlab-ci test runner.
