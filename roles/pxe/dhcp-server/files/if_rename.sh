#!/bin/sh
set -ex

# called from ansible playbook with the mac of eth-uplink and eth-local.

# does the following:
# downs both interfaces based on mac
# restart systemd-udev-trigger - to rename them
# brings both up using their new names: eth-uplink and eth-local

mac_to_name () {
    mac=$1
    # breakdown:

    # mac "aa:bb..." is passed in, set to mac.

    # ip address - dumps current ip config.

    # jq --arg mac ${mac} - stores the "aa:bb..." value to jq's mac var,
    #   (I couldn't figture out how to reference the shell var in quotes.)

    # .[] | select(.address == $mac) - item(s) where mac==mac
    # .ifname - the interface name (eth0) (in quotes)

    # --raw-output - no quotes

    ip -json link | jq --arg mac ${mac} '.[] | select(.address == $mac).ifname' --raw-output
}

for mac in $@; do
  ip link set $(mac_to_name ${mac}) down
done

systemctl restart systemd-udev-trigger.service
systemctl restart systemd-udev-settle.service

for mac in $@; do
  ip link set $(mac_to_name ${mac}) up
done

systemctl restart networking.service

