# prometheus/grafana

This role sets up a Grafana server to display Prometheus data. It enables
anonymous read access to dashboards.

## Manual Setup

This role only does the basic install, there are still a few manual
steps to perform:

### Creating SSO users:

1. Log in via SSO to create the SSO user.

### Configuring prometheus as a source:

1. Log in as an admin.
1. Add a new Data Source: `https://grafana.example.net/datasources/new`
1. Select Prometheus.
1. Use `http://localhost:9090` as the URL.
1. Save the new source.
1. Optionally import some dashboards to monitor Prometheus performance
   from the Dashboards tab: "Prometheus 2.0 Stats" and "Grafana metrics".

### Importing dashboards:

1. Import the [Node Exporter Full](https://grafana.com/grafana/dashboards/1860)
   dashboard from grafana.com by ID `1860`.
1. Import DebConf Video dashboards from JSON backups. e.g.
   [DebConf 20](https://salsa.debian.org/debconf-team/public/data/dc20-online/-/tree/master/grafana)

## Tasks

Everything is in the `tasks/main.yml` file.

## Available variables

The main variables for this role are:

* `grafana_admin_password`:       The `admin` user password for the Grafana instance.

* `grafana_server_name`:          The FQDN of the https web sever that Grafana should run.
                                  If not specified, access is only via port 9090.

* `grafana_self_signed_certificate`: Boolean. Generate a self-signed certificate instead of a LetsEncrypt certificate.

* `grafana_gitlab_sso`:           If this block is provided, SSO through GitLab will be possible.

* `grafana_gitlab_sso.base_url`:  The URL to the root of the GitLab install.

* `grafana_gitlab_sso.client_id`: OAuth2 Client ID. This application should have the `read_user` scope.

* `grafana_gitlab_sso.client_id`: OAuth2 Client Secret.
* `grafana_gitlab_sso.allowed_groups`: The GitLab groups to allow sign up from (comma separated)

* `letsencrypt_well_known_dir`:   Directory where to store the `/.well-known/` data
                                  for the Let's Encrypt `http-01` challenge.

* `prometheus_targets_directory`: Directory on the prometheus server to store exported targets in.
