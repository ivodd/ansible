# xfce-desktop

Manage an XFCE/Xorg/LightDM-based desktop setup and other related GUI applications

## Tasks

The tasks are divided this way:

* `tasks/lightdm.yml:` Manages LightDM.

* `tasks/packages.yml`: Installs video-related packages.

* `tasks/scripts.yml`: Configures useful scripts.

* `tasks/xfce4.yml`: Configures XFCE.

* `tasks/xorg.yml`: Configures Xorg server.

* `tasks/grabber.yml`: Configures the X source capture.

## Available variables

Main variables are:

* `user_name`: The username of the main user.

* `autologin`: Boolean. If false, the LightDM autologin is turned off.

* `grabber_enable`: Boolean. If true, the main X display will be captured and
                    streamed to the voctocore machine as a source.

* `grabber_host`: Hostname of the voctocore machine to stream to.

* `grabber_port`: Port of the voctocore machine to stream to.

Other variables used are:

* `skip_unit_test`:  Used internally by the test suite to disable actions that
                     can't be performed in the gitlab-ci test runner.
