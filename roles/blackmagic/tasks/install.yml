---

- name: Get available kernel versions
  ansible.builtin.command: linux-version list
  register: linux_versions

- name: Install blackmagic build deps
  ansible.builtin.apt:
    pkg:
      - linux-headers-amd64

- name: Install kernel headers for all available linux versions
  ansible.builtin.apt:
    pkg:
      - "linux-headers-{{ item }}"
  loop: "{{ linux_versions.stdout_lines }}"

- name: Install packages
  ansible.builtin.apt:
    deb: "{{ item.value }}"
  loop: "{{ blackmagic.urls | dict2items }}"

- name: Install blackmagic DKMS driver
  ansible.builtin.command:
    dkms install {{ item[0] }}/{{ blackmagic.dkms_version }} -k {{ item[1] }}
  args:
    creates: /lib/modules/{{ item[1] }}/updates/dkms/{{ item[0] }}.ko
  loop: >-
    {{
        ['blackmagic', 'blackmagic-io']
      | product(linux_versions.stdout_lines)
      | list
    }}
  when: blackmagic.dkms_version is defined

- name: Stop blackmagic GUI from being annoying
  ansible.builtin.file:
    path: /etc/xdg/autostart/{{ item }}.desktop
    state: absent
  with_items:
    - DesktopVideoUpdaterAutoStart
    - BlackmagicFirmwareUpdaterGuiAutoStart

- name: Install DKMS auto MOK script
  ansible.builtin.copy:
    src: files/mokautoenroll
    dest: /usr/local/sbin/mokautoenroll
    mode: ugo=rx

- name: Install DKMS auto MOK service
  ansible.builtin.copy:
    src: files/mokautoenroll.service
    dest: /etc/systemd/system/mokautoenroll.service

- name: Enable DMKS auto MOK service
  ansible.builtin.systemd_service:
    daemon_reload: true
    name: mokautoenroll.service
    state: started
    enabled: true
