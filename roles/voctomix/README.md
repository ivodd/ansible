# voctomix

Manage and configure the voctomix live editing software.

## Tasks

The tasks are divided this way:

* `tasks/rtmp.yml`: Manage RTMP streaming to the streaming backend machine.

* `tasks/scripts.yml`: Manage useful scripts for the video director.

* `tasks/tallylight.yml`: Manage the tally lights.

* `tasks/video_disk.yml`: Manage partitions to record to.

* `tasks/voctomix.yml`: Install and configure voctomix.

## Available variables

Main variables are:

* `user_name`: Main user username.

* `storage_username`: Storage user username.

* `debian_version`: Version of Debian, when using Debian.

* `org`: Name of your organisation. Used in video files path.

* `show`: Name of the event. Used in the video files path.

* `room_name`: Name of the room where you are recording. Used in the video file
               path.

* `frequency`: The local frequency setting (50 or 60Hz). Used to derive
               sensible defaults.

* `blackmagic_default_mode`: Default value for `blackmagic_sources.*.mode`,
                             computed from `framerate`.

* `voctomix.auto_copy_recordings`: Boolean. Adds a cron job to run the
                                   recordings copy script twice per hour.

* `voctomix.audio_mix_matrix`: String (optional). Passed to voctocore to configure its
                               audio mixing matrix. Rows are separated by a slash, columns
                               by a space (e.g. `"1.0/1.0"` to map the first input channel
                               onto the two output channels; `"1.0 0.0/0.0 1.0"` is the
                               (default) identity matrix mapping channel 0 to channel 0
                               and channel 1 to channel 1)

* `voctomix.autostart_gui`: Start Voctoguio when X starts use.

* `voctomix.framesize`: List of Integers. [Width, Height]

* `voctomix.framerate`: Integer. Number of frames per second to record at.
                        Defaults to `frequency/2`

* `voctomix.previews`: Boolean. Generate (and use) down-sampled preview
                       streams, for the GUI.

* `voctomix.mirrors`: Boolean. Mirror video sources on the 1300X TCP port
                      range, for vogol.

* `voctomix.bgimage_url`: URL of the background .png image file.

* `voctomix.recordings_port`: Optional. Voctocore port to stream recordings to.

* `recording_format`: `mpeg2` (more IO, less CPU) or `h.264` (more CPU, less IO)

* `recording_timezone`: Optional. Record timestamps in the specified TZ.

* `streaming.method`: Streaming method used. At the moment, only `none` and
                      `rtmp` values are supported.

* `streaming.hq_config.video_bitrate`: Integer. Video streaming bitrate in kbps.

* `streaming.hq_config.audio_bitrate`: Integer. Audio streaming bitratein in bps.

* `streaming.hq_config.keyframe_period`: Integer. Seconds between the creation
                                         of key frames.

* `streaming.rtmp.location`: RTMP URL to the streaming endpoint. For YouTube,
                             this would be: `rtmp://a.rtmp.youtube.com/live2/x/SUPER_SECRET_KEY app=live2`

* `streaming.rtmp.vaapi`: Boolean. Use HW-accelerated x264 encoder.

* `streaming.mix_channels`: Boolean. When true, equally merge the two channels.
                            When false, strip the right channel.

* `ingest_sources.*`: Array of sources serviced by outcasts-ingest. Similar to
                      blackmagic_sources and opsis-ingest, but very generic.

* `ingest_sources.description`: Human readable clue, used as the systemd
  service description.

* `ingest_sources.src`: All of the source elements of a gst pipeline. More
  flexable but more cumbersome than having ingest construct the string. If
  specified, the audio and video options below are ignored.

* `ingest_sources.video_source`: The family of video device, passed to
                                 `--video-source.`

* `ingest_sources.video_attribs`: gst attributes of the source element.

* `ingest_sources.video_delay`: Delay in ms for the video capture.

* `ingest_sources.video_elements`: Video transformation elements.

* `ingest_sources.audio_source`: The family of audio device.

* `ingest_sources.audio_attribs`: gst attributes of the source element.

* `ingest_sources.audio_delay`: Delay in ms for the audio capture.

* `ingest_sources.audio_elements`: Audio transformation elements.

* `ingest_sources.port`: Voctocore port to send to.

* `rsync_excludes`: List. Paths to exclude during the rsync copy of the video
                    files.

* `rsync_sshopts`: Rsync SSH options.

* `video_disk`: Partition to create and mount on `/srv/video`.

* `video_blinder`: Dict. Blinder source for voctomix. See the voctomix
                   documentation for more information configurating sources.

* `video_sources`: Dict. 'Main' sources for voctomix. See the voctomix
                   documentation for more information configurating sources.

* `voctolights.*`: Array. Uses the `camera`, `port` and `host` parameters as
                   described below. The exact syntax can be found in the
                   defaults file.

* `voctolights.camera`: Name of the camera sources, as described in the
                        `sources` variable.

* `voctolights.host`: Hostname of the voctocore.

* `voctolights.plugin`: Type of tally light. `tomu` or `serial_dtr`.

* `voctolights.port`: For `serial_dtr` lights, the serial port to attach the
                      tally light to.

Other variables used are:

* `skip_unit_test`:  Used internally by the test suite to disable actions that
                     can't be performed in the gitlab-ci test runner.
