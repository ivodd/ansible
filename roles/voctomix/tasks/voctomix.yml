---
- name: Install voctomix & related packages
  ansible.builtin.apt:
    name:
      - voctomix-outcasts
      - gstreamer1.0-pulseaudio
      - gstreamer1.0-gl
      - systemd
      # Don't install from the archive, as we're using a more recent version
      # that hasn't been backported yet.
      #  - voctomix

# This isn't pretty, but since python3-vocto, voctomix-core and voctomix-gui
# need to be installed at the same time, there's no other way to proceed.
# Yes, I've tried :O
- name: Install voc2mix packages
  ansible.builtin.apt:
    deb: "{{ item.value }}"
    force: true
    force_apt_get: true
  loop: "{{ voctomix.deburls | dict2items }}"
  when: not skip_unit_test

# This is ugly.
# See https://salsa.debian.org/debconf-video-team/ansible/-/issues/124
- name: Modify the voctogui UI - remove the audio slider
  ansible.builtin.lineinfile:
    path: /usr/share/voctomix/voctogui/ui/audio.ui
    search_string: "{{ item.search_string }}"
    line: "{{ item.line }}"
  loop:
    - {search_string: 'lower">-20',
       line: '    <property name="lower">-0</property>'}
    - {search_string: 'upper">10',
       line: '    <property name="upper">0</property>'}
  when: not skip_unit_test

# This is ugly.
# See https://salsa.debian.org/debconf-video-team/ansible/-/issues/124
- name: Modify the voctogui UI - remove the clock
  ansible.builtin.lineinfile:
    path: /usr/share/voctomix/voctogui/lib/studioclock.py
    search_string: 'self.set_size_request(130, 50)'
    line: '        self.set_size_request(0, 0)'
  when: not skip_unit_test

- name: Create voctomix config dir
  ansible.builtin.file:
    path: /etc/voctomix
    state: directory

- name: Configure voctomix
  ansible.builtin.template:
    src: templates/voctocore.ini.j2
    dest: /etc/voctomix/voctocore.ini

- name: Create recording directory
  ansible.builtin.file:
    state: directory
    dest: /srv/video/{{ org }}/{{ show }}/dv/{{ room_name }}
    recurse: true
    owner: "{{ user_name }}"
    group: "{{ user_name }}"

- name: Push the monitoring script
  ansible.builtin.copy:
    src: files/monitor-stdout
    dest: /usr/local/bin/monitor-stdout
    mode: "0755"

- name: Push the h.264 recording script
  ansible.builtin.copy:
    src: files/videoteam-record-timestamp-mp4
    dest: /usr/local/bin/videoteam-record-timestamp-mp4
    mode: "0755"

- name: Push the midnight mkdir script
  ansible.builtin.copy:
    src: files/videoteam-create-tomorrow
    dest: /usr/local/bin/videoteam-create-tomorrow
    mode: "0755"

- name: Install voctocore systemd unit
  ansible.builtin.copy:
    src: files/systemd/videoteam-voctocore.service
    dest: /etc/systemd/user/videoteam-voctocore.service
  notify: Reload-systemd

- name: Create loop directory
  ansible.builtin.file:
    path: /var/cache/voctomix
    state: directory
    mode: "0775"

- name: Download the background image
  ansible.builtin.get_url:
    url: "{{ voctomix.bgimage_url }}"
    dest: /var/cache/voctomix/background.png
  when: voctomix.bgimage_url is defined
  tags:
    - loop

- name: Remove the background image
  ansible.builtin.file:
    path: /var/cache/voctomix/background.png
    state: absent
  when: voctomix.bgimage_url is undefined
  tags:
    - loop

- name: Install voctomix recording systemd units
  ansible.builtin.template:
    src: templates/videoteam-{{ item }}.j2
    dest: /etc/systemd/user/videoteam-{{ item }}
  with_items:
    - create-tomorrow.service
    - create-tomorrow.timer
    - record.service
  notify: Reload-systemd

- name: Enable voctomix systemd units
  ansible.builtin.file:
    state: link
    src: /etc/systemd/user/videoteam-{{ item }}
    dest: "/home/{{ user_name }}/.config/systemd/user/xlogin.target.wants\
           /videoteam-{{ item }}"
    owner: "{{ user_name }}"
    group: "{{ user_name }}"
  with_items:
    - create-tomorrow.timer
    - record.service
    - voctocore.service
  when: not skip_unit_test

- name: Autostart voctogui
  ansible.builtin.file:
    src: /usr/share/applications/voctogui.desktop
    dest: /etc/xdg/autostart/voctogui.desktop
    state: link
  # REMOVE the skip_unit_test once we are back to installing voctomix from the
  # Debian archive
  when: voctomix.autostart_gui and not skip_unit_test

- name: Autostart record watch
  ansible.builtin.template:
    src: templates/recordwatch.desktop.j2
    dest: /etc/xdg/autostart/recordwatch.desktop
    mode: "0755"
