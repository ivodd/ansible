# apt

Manage and configure apt.

## Tasks

The tasks are divided into:

* `sources.yml`:        Managing `/etc/apt/sources.list`
* `sources.d.yml`:      Managing extra repos in `/etc/apt/sources.d`
* `config.yml`:         Managing APT configuration
* `local_repo.yml`:     Managing a `local-apt-repository`, containing
                        any local debs needed.

## Available variables

Main variables are:

* `debian_source`:      The Debian source you want to use.

* `debian_components`:  Space separated list of Debian components.

* `debian_version`:     The Debian codename you want to use.

* `apt_proxy`:          Boolean variable. If set to false, it will remove the
                        apt proxy D-I set one.

* `update_apt_sources`: Boolean value. If set to false, this role won't touch
                        your apt source.list file.

* `enable_sid`:         Boolean value. Enable sid in the apt sources.

* `enable_oldstable`:   Boolean value. Enable oldstable in the apt sources.

* `enable_backports`:   Boolean value. Enable backports in the apt sources.

* `extra_debs`:         List of URLs to `.deb` files to make available to APT.

* `enable_security_upgrades`:    Boolean. Enable unattended-upgrades for
                                 stable-security. If set to true, updates will
                                 be installed each day at 03:00, system time.

* `enable_stable_upgrades`: Boolean. Enable unattended-upgrades for
                            stable upgrades. If set to true, updates will be
                            installed each day at 03:00, system time.

Other variables used are:

* `skip_unit_test`:  Used internally by the test suite to disable actions that
                     can't be performed in the gitlab-ci test runner.
