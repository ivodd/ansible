# nfs-client

Installs and mounts NFS shares in `/srv`, based on the config in the
`nfs-server` role.

## Tasks

Everything is in the `tasks/main.yml` file.

## Available variables

* `nfs_server`: Name of your NFS mount. This variable is used in the NFS mount
                path.

Other variables used are:

* `skip_unit_test`:  Used internally by the test suite to disable actions that
                     can't be performed in the gitlab-ci test runner.
