# Firewall

Very simple nftables firewall.
Permits complete access from internal networks, and access to specific
ports from other sources.

## Available variables

Main variables are:

* `firewall_internal_networks`:  List of networks (CIDR) to allow all traffic from.
* `firewall_permit_ssh`:         Boolean. Permit ssh from anywhere, safely before custom rules.
* `firewall_permit_icmp`:        Boolean. Permit ICMP (v4/6) from anywhere.
* `firewall_permit_dhcp`:        Boolean. Permit DHCP from anywhere.
* `firewall_rules`:              List of firewall rules to allow.
* `firewall_rules.[].source`:    Optional. Source network (CIDR).
* `firewall_rules.[].proto`:     Optional, required if port is specified. Protocol name. (`tcp` or `udp` only)
* `firewall_rules.[].dports`:    Optional. List of destination ports.

Other variables used are:

* `skip_unit_test`:  Used internally by the test suite to disable actions that
                     can't be performed in the gitlab-ci test runner.
