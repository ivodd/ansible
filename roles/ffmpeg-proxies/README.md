# ffmpeg-proxies

Run ffmpegs that proxy data to somewhere.

## Tasks

The tasks are divided this way:

* `tasks/main.yml`: Manage the ffmpeg proxies.

* `tasks/scripts.yml`: Manage useful scripts for the video director.

* `tasks/tallylight.yml`: Manage the tally lights.

* `tasks/video_disk.yml`: Manage partitions to record to.

* `tasks/voctomix.yml`: Install and configure voctomix.

## Available variables

Main variables are:

* `ffmpeg_proxies`: Dict of proxy configuration, keyed by id of the proxy.

* `ffmpeg_proxies.[].input`: String. URL / ffmpeg source string for the
                             input.

* `ffmpeg_proxies.[].host`: String. Destination host name / IP.

* `ffmpeg_proxies.[].port`: Integer. Destination port.

* `user_name`: Main user username.
