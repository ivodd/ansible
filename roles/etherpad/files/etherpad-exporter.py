#!/usr/bin/python3
# Manged by ansible.
"""Bulk export etherpads from a Pentbarf XML file."""
# List pads from Postgres:
# SELECT DISTINCT split_part(key, ':', 2) as pad FROM store WHERE key LIKE 'pad:%' ORDER BY pad;

import json
import os
import re
import time
import urllib.request

import xml.etree.ElementTree as ET

from argparse import ArgumentParser


def get_ids_from_penta(penta_url):
    """Extract talk IDs from the Pentabarf XML."""
    talk_ids = []
    with open(penta_url) as xml:
        schedule = ET.XML(xml.read())
        for day in schedule:
            for room in day:
                for row in room:
                    talk = re.sub('^http.*/talks/', '', row.find('url').text)[:-1]
                    if re.match('^[0-9]', talk):
                        split_talk = re.split('-', talk, maxsplit=1)
                        tid = split_talk[0]
                        slug = split_talk[1]
                        if len(slug) > 33:  # Slugs are truncated at 32 chars
                            talk = tid + '-' + slug[:45]
                        talk_ids.append(talk)
    return talk_ids


def list_all_pads(hostname, apikey):
    """List all known etherpad pads, using the Etherpad API"""
    url = ('https://' + hostname + '/api/1.2.1/listAllPads?'
           + urllib.parse.urlencode({'apikey': apikey}))
    with urllib.request.urlopen(url) as f:
        response = json.load(f)
        return response['data']['padIDs']


def download(hostname, pad_id, format, output):
    """Download an etherpad file."""

    final_url = 'https://' + hostname + '/p/' + pad_id + '/export/' + format
    file_name = os.path.abspath(output) + '/' + pad_id + '.' + format
    attempts = 3
    for attempt in range(attempts):
        try:
            urllib.request.urlretrieve(final_url, file_name)
        except urllib.error.HTTPError as e:
            if e.code == 404:
                print(final_url + ': HTTP Error 404: Not Found')
                return
            if e.code == 403:
                print(final_url + ": HTTP Error 403: Presuming the pad doesn't exist")
                return
            elif e.code < 500:
                print(f'{final_url}: HTTP Error {e.code}: {e.reason}')
                raise
            elif attempt == attempts - 1:
                print(f'{final_url}: HTTP Error {e.code}: {e.reason} '
                      f'(retried {attempts} times)')
                raise


def main():
    """Main function."""
    p = ArgumentParser(description=__doc__)
    p.add_argument('-u', '--url', type=str, help='FQDN of the etherpad instance.')
    p.add_argument('-p', '--penta', help='Pentabarf XML file. Provide this or an API key.')
    p.add_argument('-a', '--apikey', help='API key. Provide this or a Penta URL')
    p.add_argument('-f', '--format', choices=['txt', 'html', 'etherpad'],
                   help='Format of the exported files.')
    p.add_argument('-o', '--output', default=os.getcwd(),
                   help='Output directory to save files to.')
    p.add_argument('-t', '--time', type=int, default="6",
                   help=("Time in seconds between file downloads. Might need to be "
                         "tweaked depending on the server's settings."))
    args = p.parse_args()

    if args.penta:
        pad_ids = get_ids_from_penta(args.penta)
    elif args.apikey:
        pad_ids = list_all_pads(args.url, args.apikey)
    else:
        p.error("Must specifiy either --penta or --apikey, to locate pads")

    if not os.path.exists(args.output):
        os.makedirs(args.output)
    for pad_id in pad_ids:
        download(args.url, pad_id, args.format, args.output)
        time.sleep(args.time)


if __name__ == "__main__":
    main()
