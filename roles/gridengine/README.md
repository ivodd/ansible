# name of the role

These roles install the gridengine-master and gridengine-exec packages
on a (set of) hosts, and configure the two for use of SReview.

## Tasks

There are two main tasks, and two "worker" tasks.

- gridengine/master is for the host that will run gridengine-master.
  There can be only one.
- gridengine/exec is for the hosts that will run gridengine-exec. There
  can be multiple ones, and it can include the host running
  gridengine-master, if desired.

The two main tasks don't have any tasks, they just set up variables for
the two "worker" tasks, and then include them via dependencies. In order
to do this, they make the assumption that the gridengine master host is
assigned to a "ge-master" group, and that the hosts that will run
gridengine-exec are assigned to an "encoder" group in the inventory.

In that light, it makes sense to assign the gridengine/master role to
the ge-master group, and the gridengine/exec role to the encoder group.

The two worker roles are:

- gridengine/install: this installs the necessary packages and the
  gridengine local fact which calls `qconf` with various options to
  detect how gridengine is currently configured.
- gridengine/config: this uses the gridengine local fact to figure out
  what needs to be configured, and creates a hostgroup to which all the
  exechosts are added, two queues (assigned to the hostgroup), and an
  smp parallel environment, in gridengine.

## Available variables

Main variables are:

* `cpu_count`: defaults to `ansible_processor_count`

* `cpu_cores`: defaults to `ansible_processor_cores`

The idea is that `cpu_count * cpu_cores` results in the number of
physical cores installed to the system, not counting hyperthreading.

In case you do not want to fully use your system for some reason, set
either `cpu_count` or `cpu_cores` (or both) to a value lower than what
your hardware supports.
